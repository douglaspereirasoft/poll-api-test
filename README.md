# Poll API #
Poll api for Sicredi test

* Made by Douglas F. Pereira,  03/2021
* version: 1.1.0


### How do I get set up? ###
* Install JDK-11
* Install mongodb v4.4.3
* Run application: ```gradlew bootRun```

### API DOC
* Swagger: http://localhost:9000/swagger-ui/
* [Postman](www.postman.com) collection: 
  * Import collection from *postman_collection.json* file
  * Import environment variables from *local.postman_collection.json* file