package com.poll.poll.repository;

import com.poll.poll.models.Poll;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollRepository extends ReactiveMongoRepository<Poll, String> {
}
