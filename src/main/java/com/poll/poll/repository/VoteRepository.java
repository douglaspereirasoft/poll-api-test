package com.poll.poll.repository;

import com.poll.poll.models.Vote;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface VoteRepository extends ReactiveMongoRepository<Vote, String> {
    Mono<Vote> findByCpfAndPollId(String cpf, String pollId);
}
