package com.poll.poll.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Document
@NoArgsConstructor
@SuperBuilder
public class Poll extends Model {
    private String topic;
    private LocalDateTime startedAt;
    private LocalDateTime closedAt;
    private boolean opened;
    private Long positiveVotes;
    private Long negativeVotes;
}
