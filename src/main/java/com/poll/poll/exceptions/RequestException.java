package com.poll.poll.exceptions;

public class RequestException extends Exception {
    public RequestException(String error) {
        super(error);
    }
}
