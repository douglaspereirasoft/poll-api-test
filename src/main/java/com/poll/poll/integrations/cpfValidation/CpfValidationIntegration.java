package com.poll.poll.integrations.cpfValidation;

import com.poll.poll.requests.AbleToVoteStatusRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class CpfValidationIntegration {
    private final String userApiUrl = "https://user-info.herokuapp.com/users/";
    private final WebClient userCheckWebClient;


    public CpfValidationIntegration() {
        this.userCheckWebClient = WebClient.create(userApiUrl);
    }

    public Mono<AbleToVoteStatusRequest> validate(String cpfDigits) {
        return userCheckWebClient
                .get()
                .uri(cpfDigits)
                .retrieve()
                .bodyToMono(AbleToVoteStatusRequest.class);
    }
}
