package com.poll.poll.utils;

public class Format {
    public static String onlyDigits(String value) {
        if (value == null) return null;
        return value.replaceAll("[\\D]", "");
    }
}
