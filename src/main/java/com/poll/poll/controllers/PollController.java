package com.poll.poll.controllers;

import com.poll.poll.exceptions.RequestException;
import com.poll.poll.requests.PollRequest;
import com.poll.poll.requests.PollStartRequest;
import com.poll.poll.requests.VoteRequest;
import com.poll.poll.responses.PollCreateResponse;
import com.poll.poll.responses.PollResponse;
import com.poll.poll.responses.VoteResponse;
import com.poll.poll.services.poll.PollService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/polls")
public class PollController {

    private static final String ERROR_POLL_ID_NOT_EXISTS = "error.poll.id.not.exists";
    private final PollService pollService;

    @Autowired
    public PollController(PollService pollService) {
        this.pollService = pollService;
    }

    @GetMapping
    @ApiOperation(value = "Get all polls")
    public Flux<PollResponse> getPolls() {
        return pollService.listPolls();
    }

    @PostMapping
    @ApiOperation(value = "Create a new poll")
    public Mono<PollCreateResponse> postPoll(@RequestBody PollRequest pollRequest) {
        return pollService.addPoll(pollRequest);
    }

    @PostMapping(value = "/{id}/start")
    @ApiOperation(value = "Start session on a poll")
    public Mono<PollResponse> startPoll(@PathVariable String id, @RequestBody PollStartRequest pollStartRequest) {
        return pollService.startPoll(id, pollStartRequest);
    }

    @PostMapping(value = "/{id}/vote")
    @ApiOperation(value = "Vote on a open poll")
    public Mono<VoteResponse> voteOnPoll(@PathVariable String id, @RequestBody VoteRequest voteRequest) {
        return pollService.voteOnPoll(id, voteRequest)
                .switchIfEmpty(Mono.error(new RequestException(ERROR_POLL_ID_NOT_EXISTS)));
    }

}
