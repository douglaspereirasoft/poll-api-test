package com.poll.poll.requests;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AbleToVoteStatusRequest {
    private String status;
}
