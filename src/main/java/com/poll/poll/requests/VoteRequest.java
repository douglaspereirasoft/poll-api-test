package com.poll.poll.requests;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VoteRequest {
    private String cpf;
    private boolean value;
}