package com.poll.poll.services.poll;

import com.poll.poll.exceptions.NotFoundException;
import com.poll.poll.exceptions.RequestException;
import com.poll.poll.integrations.cpfValidation.CpfValidationIntegration;
import com.poll.poll.models.Poll;
import com.poll.poll.models.Vote;
import com.poll.poll.repository.PollRepository;
import com.poll.poll.repository.VoteRepository;
import com.poll.poll.requests.AbleToVoteStatusRequest;
import com.poll.poll.requests.PollRequest;
import com.poll.poll.requests.PollStartRequest;
import com.poll.poll.requests.VoteRequest;
import com.poll.poll.responses.PollCreateResponse;
import com.poll.poll.responses.PollResponse;
import com.poll.poll.responses.VoteResponse;
import com.poll.poll.utils.Format;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;


@Service
@Slf4j
public class PollServiceImpl implements PollService {

    public static final String ERROR_POLL_ALREADY_OPEN = "poll.already.open";
    public static final String ERROR_POLL_NOT_OPENED = "error.poll.not.opened";
    public static final String ERROR_ALREADY_VOTED_ON_THIS_POLL = "error.already.voted.on.this.poll";
    public static final String ERROR_INVALID_CPF = "error.invalid.cpf";
    public static final String ERROR_CANNOT_CREATE_POLL_WITHOUT_TOPIC = "error.cannot.create.poll.without.topic";
    public static final String ERROR_POLL_CLOSED_AT_IN_PAST = "error.poll.closedAt.in.past";
    public static final String ABLE_TO_VOTE = "ABLE_TO_VOTE";
    public static final String UNABLE_TO_VOTE = "UNABLE_TO_VOTE";
    private final PollRepository pollRepository;
    private final VoteRepository voteRepository;
    private final ModelMapper modelMapper;
    @Value("${poll.defaultSessionTimeInMinutes:0}")
    private long defaultSessionTimeInMinutes;
    private final CpfValidationIntegration cpfValidationIntegration;

    @Autowired
    public PollServiceImpl(PollRepository pollRepository, VoteRepository voteRepository, ModelMapper modelMapper, CpfValidationIntegration cpfValidationIntegration) {
        this.pollRepository = pollRepository;
        this.voteRepository = voteRepository;
        this.modelMapper = modelMapper;
        this.cpfValidationIntegration = cpfValidationIntegration;
    }

    @Override
    public Mono<PollCreateResponse> addPoll(PollRequest pollRequest) {
        if (!StringUtils.hasText(pollRequest.getTopic())) {
            return Mono.error(new RequestException(ERROR_CANNOT_CREATE_POLL_WITHOUT_TOPIC));
        }
        Poll poll = modelMapper.map(pollRequest, Poll.class);
        log.info(String.format("added new poll: %s", poll.getTopic()));
        return pollRepository.save(poll)
                .flatMap(pollModel -> Mono.just(modelMapper.map(pollModel, PollCreateResponse.class)));
    }

    @Override
    public Mono<PollResponse> startPoll(String id, PollStartRequest pollStartRequest) {
        return pollRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException()))
                .flatMap(poll -> {
                    if (poll.isOpened()) {
                        return Mono.error(new RequestException(ERROR_POLL_ALREADY_OPEN));
                    }

                    poll.setOpened(true);
                    poll.setStartedAt(LocalDateTime.now());

                    if (pollStartRequest.getClosedAt() == null) {
                        poll.setClosedAt(LocalDateTime.now().plus(Duration.ofMinutes(defaultSessionTimeInMinutes)));
                    } else {
                        if (pollStartRequest.getClosedAt().isBefore(LocalDateTime.now())) {
                            return Mono.error(new RequestException(ERROR_POLL_CLOSED_AT_IN_PAST));
                        }
                        poll.setClosedAt(pollStartRequest.getClosedAt());
                    }

                    return pollRepository.save(poll);
                })
                .flatMap(pollModel -> Mono.just(modelMapper.map(pollModel, PollResponse.class)));
    }

    @Override
    public Mono<VoteResponse> voteOnPoll(String id, VoteRequest voteRequest) {
        return pollRepository.findById(id)
                .switchIfEmpty(Mono.error(new NotFoundException()))
                .flatMap(poll -> {
                    if (!poll.isOpened() || LocalDateTime.now().isAfter(poll.getClosedAt())) {
                        return Mono.error(new RequestException(ERROR_POLL_NOT_OPENED));
                    }
                    voteRequest.setCpf(Format.onlyDigits(voteRequest.getCpf()));

                    if (!StringUtils.hasText(voteRequest.getCpf())
                            || voteRequest.getCpf().length() != 11) {
                        return Mono.error(new RequestException(ERROR_INVALID_CPF));
                    }

                    return voteRepository.findByCpfAndPollId(voteRequest.getCpf(), poll.getId());
                })
                .flatMap(alreadyVote -> Mono.error(new RequestException(ERROR_ALREADY_VOTED_ON_THIS_POLL)))
                .switchIfEmpty(Mono.defer(() -> cpfValidationIntegration.validate(voteRequest.getCpf())))
                .switchIfEmpty(Mono.error(new NotFoundException()))
                .flatMap(apiVoteResponse -> {
                    AbleToVoteStatusRequest ableVoteStatus = (AbleToVoteStatusRequest) apiVoteResponse;
                    String status = ableVoteStatus.getStatus();
                    if (!ABLE_TO_VOTE.contentEquals(status)) {
                        return Mono.error(new RequestException(UNABLE_TO_VOTE));
                    }

                    Vote vote = modelMapper.map(voteRequest, Vote.class);
                    vote.setPollId(id);
                    return voteRepository.save(vote);
                })
                .flatMap(vote -> Mono.just(modelMapper.map(vote, VoteResponse.class)));
    }

    @Override
    public Flux<PollResponse> listPolls() {
        return pollRepository.findAll()
                .flatMap(poll -> Flux.just(modelMapper.map(poll, PollResponse.class)));
    }

    @Scheduled(fixedDelay = 60000)
    public void verifyFinishedPolls() {
        log.info("Verify opens polls");
        pollRepository.findAll()
                .filter(poll ->
                        (poll.isOpened() && poll.getClosedAt() != null && poll.getClosedAt().isBefore(LocalDateTime.now()))
                )
                .flatMap(poll -> {
                    poll.setOpened(false);

                    log.info(String.format("poll ended: (%s) %s", poll.getId(), poll.getTopic()));
                    return pollRepository.save(poll);
                })

                .subscribe();
    }
}
