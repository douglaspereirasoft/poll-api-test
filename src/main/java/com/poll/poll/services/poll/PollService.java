package com.poll.poll.services.poll;

import com.poll.poll.requests.PollRequest;
import com.poll.poll.requests.PollStartRequest;
import com.poll.poll.requests.VoteRequest;
import com.poll.poll.responses.PollCreateResponse;
import com.poll.poll.responses.PollResponse;
import com.poll.poll.responses.VoteResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PollService {
    Mono<PollCreateResponse> addPoll(PollRequest pollRequest);

    Mono<PollResponse> startPoll(String id, PollStartRequest pollStartRequest);

    Mono<VoteResponse> voteOnPoll(String id, VoteRequest voteRequest);

    Flux<PollResponse> listPolls();
}
