package com.poll.poll.responses;

import lombok.Data;

@Data
public class PollCreateResponse {
    private String id;
    private String topic;
}
