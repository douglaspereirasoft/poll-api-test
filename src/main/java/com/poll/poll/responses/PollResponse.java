package com.poll.poll.responses;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PollResponse {
    private String id;
    private String topic;
    private LocalDateTime startedAt;
    private LocalDateTime closedAt;
    private boolean opened;
}
