package com.poll.poll.services.poll;

import com.poll.poll.exceptions.NotFoundException;
import com.poll.poll.integrations.cpfValidation.CpfValidationIntegration;
import com.poll.poll.models.Poll;
import com.poll.poll.models.Vote;
import com.poll.poll.repository.PollRepository;
import com.poll.poll.repository.VoteRepository;
import com.poll.poll.requests.AbleToVoteStatusRequest;
import com.poll.poll.requests.PollRequest;
import com.poll.poll.requests.PollStartRequest;
import com.poll.poll.requests.VoteRequest;
import com.poll.poll.responses.PollCreateResponse;
import com.poll.poll.responses.PollResponse;
import com.poll.poll.responses.VoteResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.function.Predicate;

import static com.poll.poll.services.poll.PollServiceImpl.*;

@ExtendWith(SpringExtension.class)
class PollServiceImplTest {

    @InjectMocks
    private PollServiceImpl pollService;

    @Mock
    private PollRepository pollRepository;

    @Mock
    private VoteRepository voteRepository;

    @Mock
    CpfValidationIntegration cpfValidationIntegration;

    @Spy
    private ModelMapper modelMapper;

    private final String topicName = "test";
    private final String id = "0923iaj90j";
    private final String cpf = "12345678901";

    @Test
    void addPoll_Success() {

        PollRequest pollRequest = PollRequest.builder().topic(topicName).build();
        Poll poll = Poll.builder().topic(topicName).id(id).build();
        Mockito.when(pollRepository.save(Mockito.any(Poll.class)))
                .thenReturn(Mono.just(poll));

        Mono<PollCreateResponse> pollCreateResponseMono = pollService.addPoll(pollRequest);

        Predicate<PollCreateResponse> pollPredicate = pollSaved -> topicName.contentEquals(pollSaved.getTopic());
        StepVerifier.create(pollCreateResponseMono)
                .expectNextMatches(pollPredicate)
                .verifyComplete();
    }

    @Test
    void addPoll_WithoutTopic() {
        PollRequest pollRequest = PollRequest.builder().build();
        Poll poll = Poll.builder().topic(topicName).id(id).build();
        Mockito.when(pollRepository.save(Mockito.any(Poll.class)))
                .thenReturn(Mono.just(poll));

        Mono<PollCreateResponse> pollCreateResponseMono = pollService.addPoll(pollRequest);

        StepVerifier.create(pollCreateResponseMono)
                .expectError()
                .verify();
    }

    @Test
    void startPoll_Success() {
        LocalDateTime closedAt = LocalDateTime.now().plus(Duration.ofMinutes(1));
        PollStartRequest startRequest = PollStartRequest.builder().closedAt(closedAt).build();
        Poll poll = Poll.builder().topic(topicName).id(id).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class)))
                .thenReturn(Mono.just(poll));

        Mono<PollResponse> pollResponseMono = pollService.startPoll(id, startRequest);

        Predicate<PollResponse> pollPredicate = pollStarted -> closedAt.isEqual(pollStarted.getClosedAt());
        StepVerifier.create(pollResponseMono)
                .expectNextMatches(pollPredicate)
                .verifyComplete();
    }

    @Test
    void startPoll_pollNotFound() {
        LocalDateTime closedAt = LocalDateTime.now().plus(Duration.ofMinutes(1));
        PollStartRequest startRequest = PollStartRequest.builder().closedAt(closedAt).build();

        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.empty());

        Mono<PollResponse> pollResponseMono = pollService.startPoll(id, startRequest);

        StepVerifier.create(pollResponseMono)
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    void startPoll_alreadyOpen() {
        LocalDateTime closedAt = LocalDateTime.now().plus(Duration.ofMinutes(1));
        PollStartRequest startRequest = PollStartRequest.builder().closedAt(closedAt).build();
        Poll poll = Poll.builder().topic(topicName).opened(true).id(id).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));

        Mono<PollResponse> pollResponseMono = pollService.startPoll(id, startRequest);

        StepVerifier.create(pollResponseMono)
                .expectErrorMessage(ERROR_POLL_ALREADY_OPEN)
                .verify();
    }

    @Test
    void startPoll_pastDate() {
        LocalDateTime closedAt = LocalDateTime.now().plus(Duration.ofMinutes(-1));
        PollStartRequest startRequest = PollStartRequest.builder().closedAt(closedAt).build();
        Poll poll = Poll.builder().topic(topicName).id(id).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class)))
                .thenReturn(Mono.just(poll));

        Mono<PollResponse> pollResponseMono = pollService.startPoll(id, startRequest);

        StepVerifier.create(pollResponseMono)
                .expectErrorMessage(ERROR_POLL_CLOSED_AT_IN_PAST)
                .verify();
    }

    @Test
    void voteOnPoll_success() {
        VoteRequest voteRequest = VoteRequest.builder()
                .cpf(cpf)
                .value(true)
                .build();

        Poll poll = Poll.builder().topic(topicName).id(id).opened(true)
                .closedAt(LocalDateTime.now().plus(Duration.ofMinutes(1))).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));

        Mockito.when(voteRepository.findByCpfAndPollId(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Mono.empty());

        Mockito.when(cpfValidationIntegration.validate(Mockito.anyString()))
                .thenReturn(Mono.just(AbleToVoteStatusRequest.builder().status(ABLE_TO_VOTE).build()));

        Mockito.when(voteRepository.save(Mockito.any()))
                .thenAnswer(votes -> Mono.just(votes.getArgument(0)));

        Mono<VoteResponse> voteResponse = pollService.voteOnPoll(id, voteRequest);

        StepVerifier.create(voteResponse)
                .expectNext(VoteResponse.builder().cpf(cpf).value(true).build())
                .verifyComplete();
    }

    @Test
    void voteOnPoll_pollNotFound() {
        VoteRequest voteRequest = VoteRequest.builder()
                .cpf(cpf)
                .value(true)
                .build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.empty());

        Mono<VoteResponse> voteResponse = pollService.voteOnPoll(id, voteRequest);

        StepVerifier.create(voteResponse)
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    void voteOnPoll_invalidCpf() {
        VoteRequest voteRequest = VoteRequest.builder()
                .cpf("012345")
                .value(true)
                .build();

        Poll poll = Poll.builder().topic(topicName).id(id).opened(true)
                .closedAt(LocalDateTime.now().plus(Duration.ofMinutes(1))).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));

        Mono<VoteResponse> voteResponse = pollService.voteOnPoll(id, voteRequest);

        StepVerifier.create(voteResponse)
                .expectErrorMessage(ERROR_INVALID_CPF)
                .verify();
    }

    @Test
    void voteOnPoll_alreadyVote() {
        VoteRequest voteRequest = VoteRequest.builder()
                .cpf(cpf)
                .value(true)
                .build();

        Poll poll = Poll.builder().topic(topicName).id(id).opened(true)
                .closedAt(LocalDateTime.now().plus(Duration.ofMinutes(1))).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));

        Mockito.when(voteRepository.findByCpfAndPollId(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Mono.just(Vote.builder().build()));

        Mono<VoteResponse> voteResponse = pollService.voteOnPoll(id, voteRequest);

        StepVerifier.create(voteResponse)
                .expectErrorMessage(ERROR_ALREADY_VOTED_ON_THIS_POLL)
                .verify();
    }

    @Test
    void voteOnPoll_unableToVote() {
        VoteRequest voteRequest = VoteRequest.builder()
                .cpf(cpf)
                .value(true)
                .build();

        Poll poll = Poll.builder().topic(topicName).id(id).opened(true)
                .closedAt(LocalDateTime.now().plus(Duration.ofMinutes(1))).build();
        Mockito.when(pollRepository.findById(id))
                .thenReturn(Mono.just(poll));

        Mockito.when(voteRepository.findByCpfAndPollId(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(Mono.empty());

        Mockito.when(cpfValidationIntegration.validate(Mockito.anyString()))
                .thenReturn(Mono.just(AbleToVoteStatusRequest.builder().status(UNABLE_TO_VOTE).build()));


        Mono<VoteResponse> voteResponse = pollService.voteOnPoll(id, voteRequest);

        StepVerifier.create(voteResponse)
                .expectErrorMessage(UNABLE_TO_VOTE)
                .verify();
    }
}